package vc.learn.learnjdk9;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnJdk9Application implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(LearnJdk9Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Thread.currentThread().join();
    }
}
