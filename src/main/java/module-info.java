/**
 * Created on 2018-04-21 上午11:17.
 *
 * @author <a href="mailto:vague.fu@outlook.com">vague.fu</a>
 * @since 1.9
 */
module learn.jdk9 {
//    requires java.se;
//    requires java.compiler;
    requires spring.boot;
    requires spring.boot.autoconfigure;
}
